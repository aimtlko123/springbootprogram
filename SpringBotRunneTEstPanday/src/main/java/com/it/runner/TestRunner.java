package com.it.runner;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
@Component  //CREATE THE Obj
@ConfigurationProperties(prefix="my.app")  // same url
@Setter
@Getter
@ToString

public class TestRunner implements CommandLineRunner
{

	private int data;
	private String name;
	@Override
	public void run(String... args) throws Exception {
		System.out.println(this);
		

	}
	
	
	

}
