package com.it.config;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfig {
	// no of method =no of obj
	@Bean
	public CommandLineRunner clrAr()
	{
		//Interface ob=(Method Par)->{Method body}
		//CommandLineRunner  cob=(args)->System.out.println("Welcome to runner in java..");
		CommandLineRunner  cob=(String...args)->
		{System.out.println("Welcome to runner in java..");
		
		};
		return cob;
	}

}
