package in.nit.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data// its show setter and getter and to string
@NoArgsConstructor//
@AllArgsConstructor// its show all args contructor
@Entity//its mapping between emp table and db table
@Table(name="stdtab")// its showing table name or db name 
public class Student {

	@Id// its show its primary key
	@Column(name="sid")// its column name in the Table
	private Integer stdId;
	@Column(name="sname")// its column name in the Table
	private String stdName;
	@Column(name="sfee")// its column name in the Table
	private Double stdFee;
	
}
