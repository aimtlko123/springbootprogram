package in.nit.runner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import in.nit.model.Student;
import in.nit.repo.StudentRepositry;
@Component// detect the class and create the obj
public class StudentRunner implements CommandLineRunner {
 
	@Autowired
	private StudentRepositry repo;
	@Override
	public void run(String... args) throws Exception {
		repo.save(new Student(10,"raj",3500.0));
		repo.save(new Student(11,"Nisha",3536.0)); 
		repo.save(new Student(12,"Rishabh",3560.0));
		repo.save(new Student(13,"prity",3544.0));
		System.out.println("--Done--");
		System.out.println(repo.getClass().getName());
	}

}
