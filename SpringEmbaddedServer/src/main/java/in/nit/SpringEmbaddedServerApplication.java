package in.nit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringEmbaddedServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringEmbaddedServerApplication.class, args);
	}

}
