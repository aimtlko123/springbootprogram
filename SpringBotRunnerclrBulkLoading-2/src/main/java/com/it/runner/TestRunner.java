package com.it.runner;

import java.util.List;
import java.util.Set;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;


@Component //creating the Object
@ConfigurationProperties(prefix="my.app")// set the common name
public class TestRunner implements CommandLineRunner {

	private Integer Id;
	private String AppCode;
	private List<String> modules;
	private Set<String> model;
	
	public List<String> getModules() {
		return modules;
	}

	public void setModules(List<String> modules) {
		this.modules = modules;
	}

	@Override
	public void run(String... args) throws Exception {
		System.out.println(this);
		

	}

	public Integer getId() {
		return Id;
	}

	public void setId(Integer id) {
		Id = id;
	}

	public String getAppCode() {
		return AppCode;
	}

	public void setAppCode(String appCode) {
		AppCode = appCode;
	}

	public Set<String> getModel() {
		return model;
	}

	public void setModel(Set<String> model) {
		this.model = model;
	}

	@Override
	public String toString() {
		return "TestRunner [Id=" + Id + ", AppCode=" + AppCode + ", modules=" + modules + "]";
	}

	
	

}
