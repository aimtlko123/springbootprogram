package in.nit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootRestmvccCurdAngularApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootRestmvccCurdAngularApplication.class, args);
	}

}
