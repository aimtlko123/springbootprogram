package com.it;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
@Component
public class FirstAppRunner implements CommandLineRunner {

	@Override
	public void run(String... args) throws Exception {
		System.out.println("my first web Application");

	}

}
