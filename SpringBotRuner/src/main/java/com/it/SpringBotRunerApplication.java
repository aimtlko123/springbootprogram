package com.it;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBotRunerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBotRunerApplication.class, args);
	}

}
