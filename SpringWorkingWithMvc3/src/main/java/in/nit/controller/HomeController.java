package in.nit.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class HomeController {
	@RequestMapping(value = "/show",method = RequestMethod.POST)
	public String viewPage()
	{
		return "Home";
	}
	@RequestMapping("register")
   public String showReg()
   {
	   return "EmpReg";
   }
}
