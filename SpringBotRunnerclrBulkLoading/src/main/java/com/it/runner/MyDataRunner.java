package com.it.runner;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.context.properties.ConfigurationProperties;

import org.springframework.stereotype.Component;
@Component// craete Obj
@ConfigurationProperties(prefix ="my.app")// no need to write indivisual value and set common property
public class MyDataRunner implements CommandLineRunner {

	private int id;
	private String code;
	private double ver;
	

	@Override
	public void run(String... args) throws Exception {
		System.out.println(this);
	
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public double getVer() {
		return ver;
	}

	public void setVer(double ver) {
		this.ver = ver;
	}

	@Override
	public String toString() {
		return "MyDataRunner [id=" + id + ", code=" + code + ", ver=" + ver + "]";
	}

}
