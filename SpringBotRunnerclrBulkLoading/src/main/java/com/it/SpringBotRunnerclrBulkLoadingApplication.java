package com.it;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBotRunnerclrBulkLoadingApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBotRunnerclrBulkLoadingApplication.class, args);
	}

}
