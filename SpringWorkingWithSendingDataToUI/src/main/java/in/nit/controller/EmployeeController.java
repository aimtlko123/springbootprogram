package in.nit.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class EmployeeController {

	@RequestMapping("/data")
	 public String showData(Model model)
	 {
		model.addAttribute("eid" ,10);
		model.addAttribute("ename" ,"Rishabh");
		model.addAttribute("esal", 10.0);
		
	
		 
		 return "EmpData";
	 }
}
