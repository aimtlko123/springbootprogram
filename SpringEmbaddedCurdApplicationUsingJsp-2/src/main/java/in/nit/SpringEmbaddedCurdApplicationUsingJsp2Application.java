package in.nit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringEmbaddedCurdApplicationUsingJsp2Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringEmbaddedCurdApplicationUsingJsp2Application.class, args);
	}

}
