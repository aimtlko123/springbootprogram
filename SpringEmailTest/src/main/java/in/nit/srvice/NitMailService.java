package in.nit.srvice;

import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

@Component
public class NitMailService {
	
	// use has A  Relation java MailSend  and use @Autowird
	// it is method that creat MailSender craete Mime message  fn+3 see it
	@Autowired
	private JavaMailSender sender; //HAS-A;

	// Create one method sendmail
	// return type change boolean
	// 1 to -->Addres||2-->subject||3-->text//both are String type
	// 4 fileSystemResource // its working to send the file input
	public boolean sendEmail(
			String to,
			String cc[],
			String bcc[],
			String subject,
			String text,
			FileSystemResource file) {
		// using try and catch to help and handle the exception
		boolean flag=false;// if Sucessfully send then ok otherwise
		try {
			
			// write the Logic 
			// 1-Create MimeMessaage its return of MimeMessage
			MimeMessage  message=null;
			message=sender.createMimeMessage();
			
			
			
			// 2-Create helper class MimeMessage Object
			// if you want to send the attchment the true otherwise false
			// cheak file is empty or not empty false if not empty then true
			MimeMessageHelper helper=null;
			helper=new MimeMessageHelper(message, file!=null?true:false);
			
			//3-provide Messge Detail
			// its for --to--> to mail id 
			helper.setTo(to);
			// cc check cc(corbon copy) null not show
			if(cc!=null)
			helper.setCc(cc);
			// bcc check cc( Blind corbon copy) null not show
			if(bcc!=null)
			helper.setBcc(bcc);
			//  Right your text in this box
			// its by default false fn+f3
			helper.setText(text, true);
			
			// cheak file is attach or not  if there then send
			if(file!=null)
			{  //Get file name And attchment
				helper.addAttachment(file.getFilename(),file);
			}
			// set Email
			sender.send(message);
			 flag=true;
			
		} 
		catch (Exception e) {
			 flag=false;
			e.printStackTrace();
		}
        return false;
	}
	
}