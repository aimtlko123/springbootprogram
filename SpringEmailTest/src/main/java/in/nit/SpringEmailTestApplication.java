package in.nit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringEmailTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringEmailTestApplication.class, args);
	}

}
