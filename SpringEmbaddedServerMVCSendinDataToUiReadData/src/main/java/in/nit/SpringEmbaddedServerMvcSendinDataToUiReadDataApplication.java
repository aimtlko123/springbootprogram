package in.nit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringEmbaddedServerMvcSendinDataToUiReadDataApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringEmbaddedServerMvcSendinDataToUiReadDataApplication.class, args);
	}

}
