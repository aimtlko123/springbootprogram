package in.nit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringEmbaddedCurdApplicationUsingJsp1Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringEmbaddedCurdApplicationUsingJsp1Application.class, args);
	}

}
