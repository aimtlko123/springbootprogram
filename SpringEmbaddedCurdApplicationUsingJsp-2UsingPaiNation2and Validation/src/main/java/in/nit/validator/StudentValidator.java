package in.nit.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import in.nit.model.Student;
@Component
public class StudentValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return Student.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
            Student s=(Student)target;
            if(s.getStdName()==null ||s.getStdName().isBlank())
            {
            	errors.rejectValue("stdName",null, "Please Enter the Student Name;");
            }
            if(s.getStdPwd()==null || s.getStdPwd().isBlank())
            {
            	
            	errors.rejectValue("stdPwd",null, "Please Enter the Student Name;");
            }
	}

}