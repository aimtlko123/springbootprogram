package com.it;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBotRunnerclrLambdaEx1Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringBotRunnerclrLambdaEx1Application.class, args);
	}

}
