package com.it.runner;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
@Component // = Creating object in container 
public class InputReadRunner implements CommandLineRunner {

	@Value("${my.app.name-info}")// used to read the data from properties & yml file
	private String appName;
	
	@Value("${my.app.version-data}")
	private double appVer;
	
	@Override
	public void run(String... args) throws Exception {
		System.out.println(this);

	}
	@Override
	public String toString() {
		return "InputReadRunner [data=" + appName + ", appver=" + appVer + "]";
	}

}
