package in.nit;



import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;

@SpringBootApplication
public class SpringBotRemoveBannerApplication {

	public static void main(String[] args) {
//ApplicationContext ac=SpringApplication.run(SpringBotRemoveBannerApplication.class, args);
		SpringApplication ac=new SpringApplication(SpringBotRemoveBannerApplication.class);
        //ac.setBannerMode(Banner.Mode.OFF);
        ac.run(args);
	}

}
