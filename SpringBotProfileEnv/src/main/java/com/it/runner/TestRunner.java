package com.it.runner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.it.service.AlertService;
@Component

public class TestRunner implements CommandLineRunner {

	@Autowired //search obj impl class link
	private AlertService service;// has A realtion
	@Override
	public void run(String... args) throws Exception {
		System.out.println(this);

	}
	public AlertService getService() {
		return service;
	}
	public void setService(AlertService service) {
		this.service = service;
	}
	@Override
	public String toString() {
		return "TestRunner [service=" + service + "]";
	}

}
