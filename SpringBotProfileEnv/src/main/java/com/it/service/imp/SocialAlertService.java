package com.it.service.imp;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.it.service.AlertService;
@Component
@Profile("htc")
public class SocialAlertService implements AlertService {

	@Override
	public void showMsg() {
		System.out.println("From social services");

	}

}
