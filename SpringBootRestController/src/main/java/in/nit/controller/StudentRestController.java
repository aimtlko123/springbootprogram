package in.nit.controller;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StudentRestController {

	
	// 1 save the student
	@PostMapping("/save")
	public ResponseEntity<String> saveStudent()
	{
		return new ResponseEntity<String>("form save",HttpStatus.OK);
	}
	// UPDATE 
	@PutMapping("/update")
	 public ResponseEntity<String> updateStudent(){
		 return new ResponseEntity<String>("From Update ", HttpStatus.OK);
	}
	//3 delete
		 @DeleteMapping("/remove")
    public ResponseEntity<String> removeStudent()
    {
    	return new ResponseEntity<String>("FROM FETCH", HttpStatus.OK);
    }
		 @GetMapping("/all")
			public ResponseEntity<String> fetchStudent(){
				return new ResponseEntity<String>("FROM FETCH", HttpStatus.OK);
			}
}
