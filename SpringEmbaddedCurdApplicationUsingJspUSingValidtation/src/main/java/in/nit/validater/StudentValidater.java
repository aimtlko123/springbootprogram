package in.nit.validater;

import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import in.nit.model.Product;

public class StudentValidater implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		
		return Product.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {

	}

}
