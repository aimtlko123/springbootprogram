package com.it.runner;

import org.springframework.boot.CommandLineRunner;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
@Component
@Order(1)
public class JdbcTest implements CommandLineRunner,Ordered {

	@Override
	public void run(String... args) throws Exception {
	System.out.println("jdbc Test class");

	}

	@Override
	public int getOrder() {
				return 2;
	}

}
