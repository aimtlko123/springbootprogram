package com.it;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBotRunneLombokSetupApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBotRunneLombokSetupApplication.class, args);
	}

}
