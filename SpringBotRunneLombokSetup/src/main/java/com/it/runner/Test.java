package com.it.runner;

import java.util.List;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
@Component
@Setter
@Getter
@ToString
@ConfigurationProperties(prefix="my.app")
public class Test implements CommandLineRunner {

	private Integer id;
	private String code;
	private List<String> model;
	@Override
	
	public void run(String... args) throws Exception {
		System.out.println(this);
		
		

	}

}
